export const getRandomVideo = (videos, currentVideo) => {
    if (!videos) return null;

    let choice = null;

    while (!choice || choice === currentVideo) choice = videos[Math.floor(Math.random() * videos.length)];

    return choice
};