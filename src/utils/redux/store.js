import {applyMiddleware, compose, createStore} from "redux";
import rootReducer from '../../reducers/index'
import thunk from "redux-thunk";


export default function configureStore() {
    return compose(
        applyMiddleware(
            thunk
        )
    )(createStore)(rootReducer, {})
}


