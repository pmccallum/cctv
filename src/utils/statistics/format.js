export const formatStatisticsForGraph = data => {
    const formatted = [];

    data.forEach(item => {
        formatted.push({
            'date': item[0],
            'total': item[1],
        })
    });

    return formatted;
};