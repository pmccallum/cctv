import React, {useEffect, useState} from 'react';

import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import PlayerHomeContainer from "../PlayerHomeContainer/PlayerHomeContainer";
import {createMuiTheme, withStyles} from '@material-ui/core';
import styles from './styles';
import {connect} from "react-redux";
import {deepPurple} from "@material-ui/core/colors";
import AppHeader from "../AppHeader/AppHeader";

const AppContainer = props => {
    const {
        classes,
        darkMode,
    } = props;

    const [theme, setTheme] = useState(undefined);

    useEffect(() => {
        setTheme(createMuiTheme({
            palette: {
                primary: deepPurple,
                type: darkMode ? 'dark' : 'light',
            }
        }))
    }, [darkMode]);

    if (!theme) return <div />;

    return <MuiThemeProvider theme={theme}>
        <div className={classes.root}>
            <AppHeader />
            <PlayerHomeContainer/>
        </div>
    </MuiThemeProvider>
};

const mapStateToProps = state => {
    return {
        darkMode: state.ui.darkMode,
    }
};

export default withStyles(styles)(connect(mapStateToProps)(AppContainer));
