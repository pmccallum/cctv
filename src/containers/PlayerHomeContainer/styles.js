export default theme => ({
    root: {
        flex: 1,
        color: theme.palette.text.primary,
        background: theme.palette.background.default,
        transition: 'all linear 250ms',
        display: 'flex',
        '&> div': {
            display: 'flex',
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: '-80px',
        },
    },
    skipButton: {
        position: 'fixed',
        bottom: '20px',
        right: '20px',
    },
    player: {
        flex: 0,
        minWidth: '640px',
        transition: 'all ease-in-out 250ms'
        // height: '100%',
    },
    playerExpanded: {
        minWidth: '100%',
        minHeight: `calc(100% - 80px)`,
        marginBottom: '-80px',
    },
    videoInfo: {
        marginTop: '10px',
        color: theme.palette.text.hint,
        '& a': {
            color: `${theme.palette.text.secondary}!important`,
            textDecoration: 'none',
        }
    }
})
