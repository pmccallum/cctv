import React, {useEffect, useState} from 'react';
import {CircularProgress, withStyles} from "@material-ui/core";
import styles from './styles';
import {connect} from "react-redux";
import {fetchVideos, setDarkMode} from "../../actions/ui";
import YouTube from "react-youtube";
import Button from "@material-ui/core/Button";
import classNames from 'classnames';
import {SkipNextRounded} from '@material-ui/icons';
import {getRandomVideo} from "../../utils/videos/videos";

const PlayerHomeContainer = props => {
    const {
        classes,
        expandedVideo,
        fetchVideos,
        videos,
    } = props;

    const [video, setVideo] = useState(undefined);
    const [videoReady, setVideoReady] = useState(false);

    useEffect(() => {
        fetchVideos();
    }, [fetchVideos]);

    useEffect(() => {
        if (!!video || videos.length === 0) return;

        setVideo(getRandomVideo(videos, video));
    }, [videos, video]);

    const onPlayerReady = event => {
        setVideoReady(true);

        event.target.playVideo();
    };

    const onVideoEnd = () => {
        setVideo(getRandomVideo(videos, video))
    };

    const onNextClick = () => {
        if (!videoReady) return;

        setVideo(getRandomVideo(videos, video))
    };

    return <div className={classes.root}>
        {!!video &&
            <YouTube
                videoId={video.id.videoId}
                className={classNames(classes.player, {[classes.playerExpanded]: expandedVideo})}
                onReady={onPlayerReady}
                onEnd={onVideoEnd}
                onError={onVideoEnd}
                opts={{
                    playerVars: {
                        autoplay: 1,
                        controls: 0,
                    }
                }}
            />
        }

        <Button
            className={classNames(classes.skipButton)}
            color={"primary"}
            variant={"fab"}
            onClick={onNextClick}
            disabled={!video}
        >
            {!videoReady ? (
                <CircularProgress color={"white"}/>
            ) : (
                <SkipNextRounded/>
            )}
        </Button>
    </div>
};

const mapStateToProps = state => {
    return {
        darkMode: state.ui.darkMode,
        expandedVideo: state.ui.expandedVideo,
        videos: state.ui.videos,
    }
};

export default withStyles(styles)(
    connect(
        mapStateToProps,
        {
            setDarkMode,
            fetchVideos,
        }
    )(PlayerHomeContainer)
);