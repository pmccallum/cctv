export default theme => ({
    root: {
        background: theme.palette.background.paper,
        color: theme.palette.text.primary,
        display: 'flex',
        transition: 'all linear 250ms',
    },
    left: {
        flex: 1,
        display: 'flex',
        justifyContent: 'flex-start',
    },
    centre: {
        flex: 1,
        display: 'flex',
        justifyContent: 'center',
    },
    right: {
        flex: 1,
        display: 'flex',
        justifyContent: 'flex-end',
    },
    pullRight: {
        float: 'right',
    }
});