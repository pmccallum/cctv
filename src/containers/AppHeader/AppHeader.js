import React, {useEffect, useState} from 'react';
import {Toolbar, withStyles} from "@material-ui/core";
import styles from './styles';
import {connect} from "react-redux";
import {setDarkMode, setExpandedVideo} from "../../actions/ui";
import IconButton from "@material-ui/core/IconButton";
import {
    WbSunnyRounded,
    FullscreenRounded,
    FullscreenExitRounded,
    BarChartRounded,
} from "@material-ui/icons";
import Typography from "@material-ui/core/Typography";
import Badge from "@material-ui/core/Badge";
import TotalsGraphComponent from "../../components/TotalsGraphComponent/TotalsGraphComponent";
import useInterval from "../../utils/hooks/useInterval";


const AppHeader = props => {
    const {
        classes,
        darkMode,
        setDarkMode,
        expandedVideo,
        setExpandedVideo,
        totalVideos,
    } = props;

    const [reloadTimer, setReloadTimer] = useState(null);
    const [showStats, setShowingStats] = useState(false);

    useInterval(() => {
        console.info("Reloading page. Scheduled update.");
        document.location.reload();
    }, reloadTimer);

    useEffect(() => {
        if (document.location.hash === '#dark') {
            setExpandedVideo(true);
            setReloadTimer(300000);
            setDarkMode(true);
        }
    }, [setExpandedVideo, setDarkMode]);

    return <Toolbar className={classes.root}>
        <div className={classes.left}>
            {!!totalVideos &&
            <Typography variant={"caption"}>{totalVideos.toLocaleString()} videos</Typography>
            }
        </div>
        <div className={classes.centre}>
            <h1>CCTV</h1>
        </div>
        <div className={classes.right}>
            <Badge color={"primary"} badgeContent={"New!"}>
            <IconButton
                color={darkMode ? "white" : "primary"}
                onClick={() => setShowingStats(true)}
                className={classes.pullRight}
            >
                <BarChartRounded />
            </IconButton>
            </Badge>
            <IconButton
                color={darkMode ? "white" : "primary"}
                onClick={() => setExpandedVideo(!expandedVideo)}
                className={classes.pullRight}
            >
                {!expandedVideo ? (
                    <FullscreenRounded/>
                ) : (
                    <FullscreenExitRounded/>
                )}
            </IconButton>
            <IconButton
                color={darkMode ? "white" : "primary"}
                onClick={() => setDarkMode(!darkMode)}
                className={classes.pullRight}
            >
                <WbSunnyRounded/>
            </IconButton>
        </div>

        <TotalsGraphComponent
            open={showStats}
            onClose={() => setShowingStats(false)}
        />
    </Toolbar>;
};

const mapStateToProps = state => {
    return {
        darkMode: state.ui.darkMode,
        expandedVideo: state.ui.expandedVideo,
        totalVideos: state.ui.totalVideos,
    }
};

export default withStyles(styles)(
    connect(
        mapStateToProps,
        {
            setDarkMode,
            setExpandedVideo,
        }
    )(AppHeader)
);