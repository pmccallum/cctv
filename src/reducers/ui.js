import {
    RECEIVE_STATISTICS_RESPONSE,
    RECEIVE_VIDEOS_RESPONSE,
    SET_DARK_MODE,
    SET_EXPANDED_VIDEO
} from "../constants/actions/ui";

const initialProps = {
    darkMode: false,
    expandedVideo: false,
    videos: [],
    statistics: null,
    totalVideos: null,
};

function ui(state=initialProps, action) {
    switch(action.type) {
        case SET_DARK_MODE:
            return {...state, darkMode: action.enabled};
        case SET_EXPANDED_VIDEO:
            return {...state, expandedVideo: action.expanded};
        case RECEIVE_VIDEOS_RESPONSE:
            return {
                ...state,
                videos: action.response.items,
                totalVideos: action.response.pageInfo.totalResults,
            };
        case RECEIVE_STATISTICS_RESPONSE:
            return {
                ...state,
                statistics: action.response,
            };
        default:
            return state;
    }
}

export default ui;




