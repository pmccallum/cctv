import React, {useEffect} from 'react';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import IconButton from "@material-ui/core/IconButton";
import {CloseRounded} from "@material-ui/icons";
import DialogContent from "@material-ui/core/DialogContent";
import CircularProgress from "@material-ui/core/CircularProgress";
import {connect} from "react-redux";
import {fetchStatistics} from "../../actions/ui";
import {Area, AreaChart, CartesianGrid, Tooltip, XAxis, YAxis} from "recharts";
import {formatStatisticsForGraph} from "../../utils/statistics/format";

const TotalsGraphComponent = props => {
    const {
        statistics,
        open,
        onClose,
        fetchStatistics,
    } = props;

    useEffect(() => {
        fetchStatistics();
    }, [fetchStatistics]);

    return <Dialog
        fullWidth
        open={open}
    >
        <DialogTitle>
            Stats

            <IconButton
                onClick={() => onClose()}
                style={{
                    float: 'right',
                    position: 'relative',
                    top: '-8px',
                }}
            >
                <CloseRounded/>
            </IconButton>
        </DialogTitle>
        <DialogContent>
            {!statistics ? (
                <div style={{
                    padding: '15px 20px',
                    textAlign: 'center',
                }}>
                    <CircularProgress/>
                </div>
            ) : (
                <div>
                    <AreaChart
                        width={500}
                        height={400}
                        data={formatStatisticsForGraph(statistics['total_history'])}
                        margin={{
                            top: 10, right: 30, left: 0, bottom: 0,
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3"/>
                        <XAxis dataKey="date"/>
                        <YAxis dataKey={"total"}/>
                        <Tooltip/>
                        <Area type="monotone" dataKey="total" stroke="#8884d8" fill="#8884d8"/>
                    </AreaChart>
                </div>
            )}
        </DialogContent>
    </Dialog>
};

export default connect(state => {
    return {
        statistics: state.ui.statistics,
    }
}, {
    fetchStatistics,
})(TotalsGraphComponent)