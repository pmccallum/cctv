import {
    RECEIVE_STATISTICS_RESPONSE,
    RECEIVE_VIDEOS_RESPONSE,
    SET_DARK_MODE,
    SET_EXPANDED_VIDEO
} from "../constants/actions/ui";

export const setDarkMode = enabled => {
    return {
        type: SET_DARK_MODE,
        enabled,
    }
};

export const setExpandedVideo = expanded => {
    return {
        type: SET_EXPANDED_VIDEO,
        expanded,
    }
};

const receiveVideosResponse = response => {
    return {
        type: RECEIVE_VIDEOS_RESPONSE,
        response,
    }
};

const receiveStatisticsResponse = response => {
    return {
        type: RECEIVE_STATISTICS_RESPONSE,
        response,
    }
};

export const fetchVideos = () => {
    return dispatch => {
        fetch('https://jsqki0towi.execute-api.ap-southeast-2.amazonaws.com/prod')
            .then(r => r.json())
            .then(response => {
                dispatch(receiveVideosResponse(response))
            })
    }
};

export const fetchStatistics = () => {
    return dispatch => {
        fetch('http://s3-ap-southeast-2.amazonaws.com/clipchamptv/statistics.json')
            .then(r => r.json())
            .then(response => {
                dispatch(receiveStatisticsResponse(response))
            }).catch(err => {
                console.warn("Stats load FAILED.");
        })
    }
};