export const SET_DARK_MODE = 'UI/set_dark_mode';
export const SET_EXPANDED_VIDEO = 'UI/set_expanded_video';
export const RECEIVE_VIDEOS_RESPONSE = 'UI/receive_videos';
export const RECEIVE_STATISTICS_RESPONSE = 'UI/receive_statistics';