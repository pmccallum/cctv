import React, {useEffect, useState} from 'react';
import './App.css';
import AppContainer from './containers/AppContainer/AppContainer.js'
import configureStore from "./utils/redux/store";
import {Provider} from "react-redux";

function App() {
  const [store, setStore] = useState(null);

  useEffect(() => {
    setStore(configureStore());
  }, []);

  if (!store) return <div>..</div>;

  return <Provider store={store}>
    <AppContainer />
  </Provider>
}

export default App;
